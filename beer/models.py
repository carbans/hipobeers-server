# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Beer(models.Model):
    nombre = models.CharField(max_length=150)
    ibu = models.